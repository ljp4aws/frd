FROM golang:alpine as build
WORKDIR /gosrc

RUN apk add --no-cache git

COPY ./src/go.mod ./go.mod
COPY ./src/go.sum ./go.sum
RUN go mod download

COPY ./src/ ./
RUN go build frd

FROM alpine:latest
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*
RUN apk add tzdata
COPY --from=build /gosrc/frd /frd
RUN chmod u+x /frd

ENTRYPOINT ["/frd"]