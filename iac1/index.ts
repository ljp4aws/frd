import * as aws from "@pulumi/aws";

// -----------------------------------------------------------------------------
// 1. Create the VPC.
const vpcName = "frd-vpc";
const vpc = new aws.ec2.Vpc(vpcName, {
	cidrBlock: "10.0.0.0/16",
	tags: {
		Name: vpcName
	}
});

const availabilityZones = aws.getAvailabilityZones({ state: "available" });
const subnets = new Array<aws.ec2.Subnet>();

for (let i = 0; i < availabilityZones.zoneIds.length; i++) {
	const subnetName = `frd-subnet-${i + 1}`;
	const subnet = new aws.ec2.Subnet(subnetName, {
		vpcId: vpc.id,
		cidrBlock: `10.0.${i}.0/24`,
		availabilityZoneId: availabilityZones.zoneIds[i],
		tags: {
			Name: subnetName
		}
	});

	subnets.push(subnet);
}

const internetGatewayName = "frd-internet-gateway";
const internetGateway = new aws.ec2.InternetGateway(internetGatewayName, {
	vpcId: vpc.id,
	tags: {
		Name: internetGatewayName
	}
});

const routeTableName = "frd-route-table";
const routeTable = new aws.ec2.RouteTable(routeTableName, {
	vpcId: vpc.id,
	routes: [
		{
			cidrBlock: "0.0.0.0/0",
			gatewayId: internetGateway.id
		}
	],
	tags: {
		Name: routeTableName
	}
});

const routeTableAssociations = new Array<aws.ec2.RouteTableAssociation>();

for (let i = 0; i < subnets.length; i++) {
	const routeTableAssociationName = `frd-route-table-association-${i + 1}`;
	const routeTableAssociation = new aws.ec2.RouteTableAssociation(routeTableAssociationName, {
		subnetId: subnets[i].id,
		routeTableId: routeTable.id
	});

	routeTableAssociations.push(routeTableAssociation);
}

// -----------------------------------------------------------------------------
// 2. Create the ECR repository for the image.
const ecrRepository = new aws.ecr.Repository("frd-ecr-repository", {
	name: "frd"
});

export const ecrRepositoryUrl = ecrRepository.repositoryUrl;

// -----------------------------------------------------------------------------
// 3. Create the IAM roles and policies.
const ec2RoleName = "frd-role-ec2";
const ec2Role = new aws.iam.Role(ec2RoleName, {
	name: ec2RoleName,
	assumeRolePolicy: {
		Version: "2012-10-17",
		Statement: [
			{
				Sid: "",
				Effect: "Allow",
				Principal: {
					Service: "ec2.amazonaws.com"
				},
				Action: "sts:AssumeRole"
			}
		]
	},
	tags: {
		Name: ec2RoleName
	}
});

const ec2InstanceProfileName = "frd-instance-profile-ec2";
const ec2InstanceProfile = new aws.iam.InstanceProfile(ec2InstanceProfileName, {
	name: ec2InstanceProfileName,
	role: ec2Role.name
});

const ec2RolePolicyName = "frd-role-policy-ec2";
const ec2RolePolicy = new aws.iam.RolePolicy(ec2RolePolicyName, {
	name: ec2RolePolicyName,
	role: ec2Role.name,
	policy: {
		Version: "2012-10-17",
		Statement: [
			{
				Effect: "Allow",
				Action: [
					"ec2:DescribeTags",
					"ecs:CreateCluster",
					"ecs:DeregisterContainerInstance",
					"ecs:DiscoverPollEndpoint",
					"ecs:Poll",
					"ecs:RegisterContainerInstance",
					"ecs:StartTelemetrySession",
					"ecs:UpdateContainerInstancesState",
					"ecs:Submit*",
					"ecr:GetAuthorizationToken",
					"ecr:BatchCheckLayerAvailability",
					"ecr:GetDownloadUrlForLayer",
					"ecr:BatchGetImage",
					"logs:CreateLogStream",
					"logs:PutLogEvents"
				],
				Resource: "*"
			}
		]
	}
});

const ecsRoleName = "frd-role-ecs";
const ecsRole = new aws.iam.Role(ecsRoleName, {
	name: ecsRoleName,
	assumeRolePolicy: {
		Version: "2012-10-17",
		Statement: [
			{
				Action: "sts:AssumeRole",
				Principal: {
					Service: "ecs.amazonaws.com",
				},
				Effect: "Allow",
				Sid: ""
			}
		]
	},
	tags: {
		Name: ecsRoleName
	}
});

const ecsRolePolicyAttachmentName = "frd-role-policy-attachment-ecs";
const ecsRolePolicyAttachment = new aws.iam.RolePolicyAttachment(ecsRolePolicyAttachmentName, {
	role: ecsRole.name,
	policyArn: "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceRole"
});