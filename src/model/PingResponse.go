package model

import "time"

// PingResponse is the response model for the Ping route.
type PingResponse struct {
	Now time.Time `json:"now"`
}
