package main

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
}

func run() error {
	c, err := buildContainer()
	if err != nil {
		return err
	}

	handler, err := buildHandler(c)
	if err != nil {
		return err
	}

	return http.ListenAndServe(":http", handler)
}
