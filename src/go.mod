module frd

go 1.13

require (
	github.com/ljpx/di v0.0.3
	github.com/ljpx/logging v0.0.1
	github.com/ljpx/web v0.0.4
)
