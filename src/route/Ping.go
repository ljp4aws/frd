package route

import (
	"net/http"
	"time"

	"github.com/ljpx/web"

	"frd/model"
)

// Ping :: GET /ping
type Ping struct{}

var _ web.Route = &Ping{}

// Method returns the HTTP method used by the route.
func (x *Ping) Method() string {
	return http.MethodGet
}

// Path returns the path used by the route.
func (x *Ping) Path() string {
	return "/path"
}

// Middleware returns the set of middleware used by the route.
func (x *Ping) Middleware() []web.Middleware {
	return nil
}

// Handle handles incoming requests for the route.
func (x *Ping) Handle(ctx *web.Context) {
	ctx.RespondWithJSON(http.StatusOK, &model.PingResponse{
		Now: time.Now(),
	})
}
