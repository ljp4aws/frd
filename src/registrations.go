package main

import (
	"frd/route"
	"log"
	"net/http"
	"os"

	"github.com/ljpx/di"
	"github.com/ljpx/logging"
	"github.com/ljpx/web"
)

func buildContainer() (di.Container, error) {
	c := di.NewContainer()

	c.Register(di.Singleton, func(c di.Container) (logging.Logger, error) {
		return log.New(os.Stdout, "", log.Ldate), nil
	})

	return c, nil
}

func buildHandler(c di.Container) (http.Handler, error) {
	var logger logging.Logger
	if err := c.Resolve(&logger); err != nil {
		return nil, err
	}

	hb := web.NewHandlerBuilder(c, logger, &web.Config{
		ProblemDetailsTypePrefix: "https://frd.ljp4aws.com/problems",
		DebuggingEnabled:         true,
		JSONContentLengthLimit:   1 << 20,
	})

	hb.Use(&route.Ping{})

	return hb.Build(), nil
}
